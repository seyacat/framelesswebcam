using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Webcam : MonoBehaviour
{
  WebCamTexture webcamTexture;
  WebCamDevice[] devices;

  int device_index = 0;
  // Start is called before the first frame update
  void Start()
  {
    if (PlayerPrefs.HasKey("device_index"))
    {
      device_index = PlayerPrefs.GetInt("device_index");
    }
    webcamTexture = new WebCamTexture();
    devices = WebCamTexture.devices;
    webcamTexture.deviceName = devices[device_index].name;
    Debug.Log(devices);
    GetComponent<RawImage>().texture = webcamTexture;
    webcamTexture.Play();
  }

  // Update is called once per frame
  void Update()
  {

  }
    public void switchCam()
  {

    device_index = (device_index + 1) % devices.Length;
    Debug.Log(device_index);
    webcamTexture.Stop();
    webcamTexture.deviceName = devices[device_index].name;
    webcamTexture.Play();
    PlayerPrefs.SetInt("device_index", device_index);
  }


}
